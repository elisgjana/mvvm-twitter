//
//  LoginController.swift
//  Twitter MVVM
//
//  Created by Admin on 18.4.21.
//

import UIKit

class LoginController: UIViewController, UITextFieldDelegate {

    // MARK: -Properties
    private let logoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "TwitterLogo")
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    private lazy var emailContainerView: UIView = {
        let view = Utilities().inputContainerView(withImage: #imageLiteral(resourceName: "ic_mail_outline_white_2x-1"), textField: emailTextField)
        return view
    }()
    
    private lazy var passwordContainerView: UIView = {
        let view = Utilities().inputContainerView(withImage: #imageLiteral(resourceName: "ic_lock_outline_white_2x"), textField: passwordTextField)
        return view
    }()
    
    private let emailTextField: UITextField = {
        let tf = Utilities().textField(withPlaceholder: "Email")
        return tf
    }()
    
    private let passwordTextField: UITextField = {
        let tf = Utilities().textField(withPlaceholder: "Password")
        tf.isSecureTextEntry = true
        return tf
    }()
    
    private let loginBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Log In", for: .normal)
        btn.setTitleColor(.twitterBlue, for: .normal)
        btn.backgroundColor = .white
        btn.layer.cornerRadius = 5
        btn.heightAnchor.constraint(lessThanOrEqualToConstant: 50).isActive = true
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        btn.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return btn
    }()
    
    private let dontHaveAccountBtn: UIButton = {
        let btn = Utilities().attributedButton("Don't have an account?", " Sign Up")
        btn.addTarget(self, action: #selector(handleShowSignUp), for: .touchUpInside)
        return btn
    }()
    
    // MARK: -Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    // MARK: -Selectors
    @objc func handleLogin(){
        
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        CustomActivityIndicator.instance.show()
        
        AuthService.shared.logUserIn(withEmail: email, password: password) { result, error in
            if let error = error{
                CustomActivityIndicator.instance.hide()
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            print("DEBUG: Success login")
            
            guard let window = UIApplication.shared.windows.first(where: {$0.isKeyWindow }) else { return }
            
            guard let tab = window.rootViewController as? MainTabController else { return }
            
            tab.authenticateUserAndConfigureUI()
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: -Helpers
    func configureUI(){
        view.backgroundColor = .twitterBlue
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isHidden = true
        
        view.addSubview(logoImageView)
        logoImageView.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 20, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 150, height: 150)
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        
        let stack = UIStackView(arrangedSubviews: [emailContainerView, passwordContainerView, loginBtn])
        stack.axis = .vertical
        stack.spacing = 30
        
        
        view.addSubview(stack)
        stack.anchor(top: logoImageView.bottomAnchor, paddingTop: 20, left: view.leftAnchor, paddingLeft: 20, bottom: nil, paddingBottom: 0, right: view.rightAnchor, paddingRight: 20, width: 0, height: 200)
        
        view.addSubview(dontHaveAccountBtn)
        dontHaveAccountBtn.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingBottom: 40, right: nil, paddingRight: 0, width: 350, height: 50)
        dontHaveAccountBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    @objc func handleShowSignUp(){
        let controller = RegistrationController()
        navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: -UITextFieldDelegate
extension LoginController{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
