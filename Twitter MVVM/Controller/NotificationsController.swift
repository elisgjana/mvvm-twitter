//
//  NotificationsController.swift
//  Twitter MVVM
//
//  Created by Admin on 18.4.21.
//

import UIKit

class NotificationsController: UIViewController {

    // MARK: -Properties
    
    // MARK: -Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    
    // MARK: -Helpers
    func configureUI(){
        view.backgroundColor = .white
        navigationItem.title = "Notifications"
    }
}
