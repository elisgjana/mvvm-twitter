//
//  Utilities.swift
//  Twitter MVVM
//
//  Created by Admin on 18.4.21.
//

import Foundation
import UIKit

class Utilities{
    
    func inputContainerView(withImage image: UIImage, textField: UITextField) -> UIView{
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let iv = UIImageView()
        iv.image = image
        iv.tintColor = .white
        
        view.addSubview(iv)
        iv.anchor(top: nil, paddingTop: 0, left: view.leftAnchor, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 25, height: 25)
        iv.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
        textField.textColor = .white
        
        view.addSubview(textField)
        textField.anchor(top: nil, paddingTop: 0, left: iv.rightAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: view.rightAnchor, paddingRight: 8, width: 0, height: 0)
        textField.centerYAnchor.constraint(equalTo: iv.centerYAnchor).isActive = true
        
        let dividerView = UIView()
        dividerView.backgroundColor = .white
        
        view.addSubview(dividerView)
        dividerView.anchor(top: textField.bottomAnchor, paddingTop: 8, left: view.leftAnchor, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: view.rightAnchor, paddingRight: 0, width: 0, height: 0.75)
        
        return view
    }
    
    func textField(withPlaceholder placeholder: String) -> UITextField {
        let tf = UITextField()
        tf.textColor = .white
        tf.font = UIFont.systemFont(ofSize: 16)
        tf.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        return tf
    }
    
    func attributedButton(_ firstPart: String, _ secondPart: String) -> UIButton{
        let btn = UIButton(type: .system)
        
        let attributedTitle = NSMutableAttributedString(string: firstPart, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white])
        
        attributedTitle.append(NSAttributedString(string: secondPart, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        btn.setAttributedTitle(attributedTitle, for: .normal)
        
        return btn
    }
}
