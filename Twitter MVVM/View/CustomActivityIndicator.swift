//
//  CustomActivityIndicator.swift
//  Twitter MVVM
//
//  Created by Admin on 1.7.21.
//

import Foundation
import Lottie

open class CustomActivityIndicator{
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.75)
        return view
    }()
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
        return view
    }()
    
    let animationView: UIView = {
        let view = UIView()
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Loading..."
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .darkGray
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Please be patient while our server is processing your request."
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = .lightGray
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let lottieAnimationView = LOTAnimationView(name: "animation-loading")
    
    open class var instance: CustomActivityIndicator {
        struct Static {
            static let instance: CustomActivityIndicator = CustomActivityIndicator()
        }
        return Static.instance
    }
    
    open func show (){
        if let window = UIApplication.shared.keyWindow{
            //setup blackView
            window.addSubview(blackView)
            blackView.anchor(top: window.topAnchor, paddingTop: 0, left: window.leftAnchor, paddingLeft: 0, bottom: window.bottomAnchor, paddingBottom: 0, right: window.rightAnchor, paddingRight: 0, width: 0, height: 0)
            
            //setup container View
            blackView.addSubview(containerView)
            containerView.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 240, height: 160)
            containerView.centerXAnchor.constraint(equalTo: blackView.centerXAnchor).isActive = true
            containerView.centerYAnchor.constraint(equalTo: blackView.centerYAnchor).isActive = true
            
            //setup animationView
            containerView.addSubview(animationView)
            animationView.anchor(top: containerView.topAnchor, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 100, height: 100)
            animationView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
            setupAnimationView(lottieAnimationView)
            
            //setup titleLabel
            containerView.addSubview(titleLabel)
            titleLabel.anchor(top: animationView.bottomAnchor, paddingTop: -25, left: containerView.leftAnchor, paddingLeft: 20, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: 20, width: 0, height: 0)

            //setup descriptionLabel
            containerView.addSubview(descriptionLabel)
            descriptionLabel.anchor(top: titleLabel.bottomAnchor, paddingTop: 10, left: containerView.leftAnchor, paddingLeft: 10, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: 10, width: 0, height: 0)
        }
        
    }
    
    fileprivate func setupAnimationView(_ lottieAnimaView: LOTAnimationView){
        //setup lotie file
        animationView.addSubview(lottieAnimaView)
        lottieAnimaView.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 0, bottom: 0)
        lottieAnimaView.contentMode = .scaleAspectFill
        lottieAnimaView.loopAnimation = true
        lottieAnimaView.play()
    }
    
    open func hide() {
        performUIUpdatesOnMain {
            self.lottieAnimationView.stop()
            self.blackView.removeFromSuperview()
        }
    }
    
    /**
     This method returns to the main que and performs all operations on that queue
     - Parameter updates : the updates that you want to perform on main
     */
    func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
        DispatchQueue.main.async {
            updates()
        }
    }
    
}
