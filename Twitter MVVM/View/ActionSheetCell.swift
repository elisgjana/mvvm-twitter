//
//  ActionSheetCell.swift
//  Twitter MVVM
//
//  Created by Admin on 4.6.21.
//

import Foundation
import UIKit

class ActionSheetCell: UITableViewCell {
    //MARK: -Properties
    
    var option: ActionSheetOptions?{
        didSet{
            configure()
        }
    }
    
    private let optionImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "twitter_logo_blue")
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        return iv
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Test Option"
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()
    
    
    //MARK: -Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        addSubview(optionImageView)
        optionImageView.anchor(top: nil, paddingTop: 0, left: leftAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 36, height: 36)
        optionImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(titleLabel)
        titleLabel.anchor(top: nil, paddingTop: 0, left: optionImageView.rightAnchor, paddingLeft: 12, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 12, width: 0, height: 0)
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: -Helpers
    
    func configure(){
        titleLabel.text = option?.description
    }
}
