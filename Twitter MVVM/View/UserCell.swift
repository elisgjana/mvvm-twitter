//
//  UserCell.swift
//  Twitter MVVM
//
//  Created by Admin on 24.5.21.
//

import Foundation
import UIKit

class UserCell: UITableViewCell {
    // MARK: - Properties
    
    var user: User? {
        didSet{
            configure()
        }
    }
    
    private let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .twitterBlue
        iv.heightAnchor.constraint(equalToConstant: 32).isActive = true
        iv.widthAnchor.constraint(equalToConstant: 32).isActive = true
        iv.layer.cornerRadius = 32 / 2
        return iv
    }()
    
    private let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "Username"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    private let fullnameLabel: UILabel = {
        let label = UILabel()
        label.text = "Full Name"
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(profileImageView)
        profileImageView.anchor(top: nil, paddingTop: 0, left: leftAnchor, paddingLeft: 12, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        
        let stack = UIStackView(arrangedSubviews: [usernameLabel, fullnameLabel])
        stack.axis = .vertical
        stack.spacing = 2
        
        addSubview(stack)
        stack.anchor(top: nil, paddingTop: 0, left: profileImageView.rightAnchor, paddingLeft: 12, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        stack.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    
    func configure(){
        profileImageView.sd_setImage(with: user?.profileImageUrl)
        usernameLabel.text = user?.username
        fullnameLabel.text = user?.fullname
    }
}
