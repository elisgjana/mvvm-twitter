//
//  TweetHeader.swift
//  Twitter MVVM
//
//  Created by Admin on 25.5.21.
//

import Foundation
import UIKit

protocol TweetHeaderDelegate: class {
    func showActionSheet()
}

class TweetHeader: UICollectionReusableView {
    // MARK: - Properties
    
    var tweet: Tweet? {
        didSet{ configure() }
    }
    
    weak var delegate: TweetHeaderDelegate?
    
    private lazy var profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .twitterBlue
        iv.heightAnchor.constraint(equalToConstant: 48).isActive = true
        iv.widthAnchor.constraint(equalToConstant: 48).isActive = true
        iv.layer.cornerRadius = 48 / 2
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleProfileImageTapped))
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        
        return iv
    }()
    
    private let fullnameLabel: UILabel = {
        let label = UILabel()
        label.text = "Waterfall"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    private let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "@waterfall"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .lightGray
        return label
    }()
    
    private let captionLabel: UILabel = {
        let label = UILabel()
        label.text = "This is some caption"
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 0
        return label
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "6:33 PM"
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        return label
    }()
    
    private lazy var optionsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = .lightGray
        btn.setImage(#imageLiteral(resourceName: "down_arrow_24pt"), for: .normal)
        btn.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
        return btn
    }()
    
    private lazy var retweetsLabel = UILabel()
    
    private lazy var likesLabel = UILabel()
    
    private lazy var commentBtn: UIButton = {
        let btn = createButton(withImageName: "comment")
        btn.addTarget(self, action: #selector(handleCommentTapped), for: .touchUpInside)
        return btn
    }()
    
    private lazy var retweetBtn: UIButton = {
        let btn = createButton(withImageName: "retweet")
        btn.addTarget(self, action: #selector(handleRetweetTapped), for: .touchUpInside)
        return btn
    }()
    
    private lazy var likeBtn: UIButton = {
        let btn = createButton(withImageName: "like")
        btn.addTarget(self, action: #selector(handleLikeTapped), for: .touchUpInside)
        return btn
    }()
    
    private lazy var shareBtn: UIButton = {
        let btn = createButton(withImageName: "share")
        btn.addTarget(self, action: #selector(handleShareTapped), for: .touchUpInside)
        return btn
    }()
    
    private lazy var statsView: UIView = {
        let view = UIView()
        
        let divider1 = UIView()
        divider1.backgroundColor = .systemGroupedBackground
        
        view.addSubview(divider1)
        divider1.anchor(top: view.topAnchor, paddingTop: 0, left: view.leftAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: view.rightAnchor, paddingRight: 8, width: 0, height: 1)
        
        let stack = UIStackView(arrangedSubviews: [retweetsLabel, likesLabel])
        stack.axis = .horizontal
        stack.spacing = 12
        
        view.addSubview(stack)
        stack.anchor(top: nil, paddingTop: 0, left: view.leftAnchor, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        stack.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        let divider2 = UIView()
        divider2.backgroundColor = .systemGroupedBackground
        view.addSubview(divider2)
        divider2.anchor(top: nil, paddingTop: 0, left: view.leftAnchor, paddingLeft: 8, bottom: view.bottomAnchor, paddingBottom: 0, right: view.rightAnchor, paddingRight: 8, width: 0, height: 1)
        
        return view
    }()
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let labelStack = UIStackView(arrangedSubviews: [fullnameLabel, usernameLabel])
        labelStack.axis = .vertical
        labelStack.spacing = -6
        
        let stack = UIStackView(arrangedSubviews: [profileImageView, labelStack])
        stack.spacing = 12
        
        addSubview(stack)
        stack.anchor(top: topAnchor, paddingTop: 16, left: leftAnchor, paddingLeft: 16, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        
        addSubview(captionLabel)
        captionLabel.anchor(top: stack.bottomAnchor, paddingTop: 20, left: leftAnchor, paddingLeft: 16, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 16, width: 0, height: 0)
        
        addSubview(dateLabel)
        dateLabel.anchor(top: captionLabel.bottomAnchor, paddingTop: 20, left: leftAnchor, paddingLeft: 16, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 16, width: 0, height: 0)
        
        addSubview(optionsButton)
        optionsButton.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 8, width: 30, height: 30)
        optionsButton.centerYAnchor.constraint(equalTo: stack.centerYAnchor).isActive = true
        
        addSubview(statsView)
        statsView.anchor(top: dateLabel.bottomAnchor, paddingTop: 12, left: leftAnchor, paddingLeft: 16, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 20, width: 0, height: 40)
        
        let actionStack = UIStackView(arrangedSubviews: [commentBtn, retweetBtn, likeBtn, shareBtn])
        actionStack.spacing = 72
        
        addSubview(actionStack)
        actionStack.anchor(top: statsView.bottomAnchor, paddingTop: 16, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        actionStack.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Selectors
    @objc func handleProfileImageTapped(){
        
    }
    
    @objc func showActionSheet(){
        delegate?.showActionSheet()
    }
    
    @objc func handleCommentTapped(){
        
    }
    
    @objc func handleLikeTapped(){
        
    }
    
    @objc func handleRetweetTapped(){
        
    }
    
    @objc func handleShareTapped(){
        
    }
    
    // MARK: - Helpers
    
    func createButton(withImageName imageName: String) -> UIButton{
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.tintColor = .darkGray
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        button.widthAnchor.constraint(equalToConstant: 20).isActive = true
        return button
    }
    
    func configure(){
        guard let tweet = tweet else { return }
        
        let vm = TweetViewModel(tweet: tweet)
        
        captionLabel.text = tweet.caption
        fullnameLabel.text = tweet.user.fullname
        usernameLabel.text = vm.usernameText
        profileImageView.sd_setImage(with: vm.profileImageUrl)
        dateLabel.text = vm.headerTimeStamp
        likesLabel.attributedText = vm.likesAttributedString
        retweetsLabel.attributedText = vm.retweetsAttributedString
    }
}
