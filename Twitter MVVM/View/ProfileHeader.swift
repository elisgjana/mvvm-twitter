//
//  ProfileHeader.swift
//  Twitter MVVM
//
//  Created by Admin on 21.5.21.
//

import Foundation
import UIKit

protocol ProfileHeaderDelegate: class {
    func handleDismissal()
    func handleEditProfileFollow(_ header: ProfileHeader)
}

class ProfileHeader: UICollectionReusableView{
    //MARK: -Properties
    var user: User? {
        didSet{ configure() }
    }
    
    weak var delegate: ProfileHeaderDelegate?
    
    private let filterBar = ProfileFilterView()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .twitterBlue
        
        view.addSubview(backButton)
        backButton.anchor(top: view.topAnchor, paddingTop: 42, left: view.leftAnchor, paddingLeft: 16, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 30, height: 30)
        
        return view
    }()
    
    private lazy var backButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "baseline_arrow_back_white_24dp"), for: .normal)
        btn.tintColor = .white
        btn.addTarget(self, action: #selector(handleDismissal), for: .touchUpInside)
        return btn
    }()
    
    private let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .lightGray
        iv.heightAnchor.constraint(equalToConstant: 80).isActive = true
        iv.widthAnchor.constraint(equalToConstant: 80).isActive = true
        iv.layer.cornerRadius = 80 / 2
        iv.layer.borderWidth = 4
        iv.layer.borderColor = UIColor.white.cgColor
        return iv
    }()
    
    lazy var editProfileFollowBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Loading", for: .normal)
        btn.layer.borderColor = UIColor.twitterBlue.cgColor
        btn.layer.borderWidth = 1.25
        btn.tintColor = .twitterBlue
        btn.addTarget(self, action: #selector(handleEditProfileFollow), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        return btn
    }()
    
    private let fullnameLabel: UILabel = {
        let label = UILabel()
        label.text = "Waterfall"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    private let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "@waterfall"
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .lightGray
        return label
    }()
    
    private let bioLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 3
        label.text = "This is the user bio which will be changed in the future to dynamic"
        return label
    }()
    
    private let underlineView: UIView = {
        let view = UIView()
        view.backgroundColor = .twitterBlue
        return view
    }()
    
    private let followingLabel: UILabel = {
        let label = UILabel()
        
        label.text = "0 Following"
        
        let followTap = UITapGestureRecognizer(target: self, action: #selector(handleAllFollowingTapped))
        label.addGestureRecognizer(followTap)
        label.isUserInteractionEnabled = true
        
        return label
    }()
    
    private let followersLabel: UILabel = {
        let label = UILabel()
        label.text = "0 Followers"
        
        let followTap = UITapGestureRecognizer(target: self, action: #selector(handleAllFollowersTapped))
        label.addGestureRecognizer(followTap)
        label.isUserInteractionEnabled = true
        
        return label
    }()
    
    //MARK: -Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        filterBar.delegate = self
 
        addSubview(containerView)
        containerView.anchor(top: topAnchor, paddingTop: 0, left: leftAnchor, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 0, width: 0, height: 108)
        
        addSubview(profileImageView)
        profileImageView.anchor(top: containerView.bottomAnchor, paddingTop: -24, left: leftAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        
        addSubview(editProfileFollowBtn)
        editProfileFollowBtn.anchor(top: containerView.bottomAnchor, paddingTop: 12, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 12, width: 100, height: 36)
        editProfileFollowBtn.layer.cornerRadius = 36 / 2
        
        let userDetailStack = UIStackView(arrangedSubviews: [fullnameLabel, usernameLabel, bioLabel])
        userDetailStack.axis = .vertical
        userDetailStack.distribution = .fillProportionally
        userDetailStack.spacing = 4
        
        addSubview(userDetailStack)
        userDetailStack.anchor(top: profileImageView.bottomAnchor, paddingTop: 8, left: leftAnchor, paddingLeft: 12, bottom: nil, paddingBottom: 0, right: rightAnchor, paddingRight: 12, width: 0, height: 0)
        
        let followStack = UIStackView(arrangedSubviews: [followersLabel, followingLabel])
        followStack.axis = .horizontal
        followStack.distribution = .fillEqually
        followStack.spacing = 8
        
        addSubview(followStack)
        followStack.anchor(top: userDetailStack.bottomAnchor, paddingTop: 8, left: leftAnchor, paddingLeft: 12, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        
        addSubview(filterBar)
        filterBar.anchor(top: nil, paddingTop: 0, left: leftAnchor, paddingLeft: 0, bottom: bottomAnchor, paddingBottom: 0, right: rightAnchor, paddingRight: 0, width: 0, height: 50)
        
        addSubview(underlineView)
        underlineView.anchor(top: nil, paddingTop: 0, left: leftAnchor, paddingLeft: 0, bottom: bottomAnchor, paddingBottom: 0, right: nil, paddingRight: 0, width: frame.width / 3, height: 2)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: -Selectors
    
    @objc func handleAllFollowersTapped(){
        
    }
    
    @objc func handleAllFollowingTapped(){
        
    }
    
    @objc func handleDismissal(){
        delegate?.handleDismissal()
    }
    
    @objc func handleEditProfileFollow(){
        delegate?.handleEditProfileFollow(self)
    }
    
    //MARK: -Helpers
    func configure(){
        guard let user = user else { return }
        
        let viewModel = ProfileHeaderViewModel(user: user)
        
        followingLabel.attributedText = viewModel.followingText
        followersLabel.attributedText = viewModel.followersText
        
        profileImageView.sd_setImage(with: user.profileImageUrl)
        fullnameLabel.text = user.fullname
        usernameLabel.text = "@\(user.username)"
        editProfileFollowBtn.setTitle(viewModel.actionButtonTitle, for: .normal)
    }
}

//MARK: -ProfileFilterViewDelegate

extension ProfileHeader: ProfileFilterViewDelegate{
    func filterView(_ view: ProfileFilterView, didSelect indexPath: IndexPath) {
        guard let cell = view.collectionView.cellForItem(at: indexPath) as? ProfileFilterCell else {
            return
        }
        
        let xPosition = cell.frame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.underlineView.frame.origin.x = xPosition
        }
    }
}
